import 'dart:async';
import 'dart:io';

Future<String> fetchData() {
  return HttpClient()
      .getUrl(Uri.parse('https://example.com/api/data'))
      .then((HttpClientRequest request) => request.close())
      .then((HttpClientResponse response) => response.transform(utf8.decoder).join());
}

void main() {
  fetchData().then((data) {
    print(data);
  });
} 