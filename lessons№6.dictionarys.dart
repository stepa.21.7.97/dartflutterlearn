// Словари, они же Maps.

// Словари представляет собой ассоциативный массив, который хранит пары ключ-значение.
// В отличие от списков и множеств, словари не имеют упорядоченности и ключи должны быть уникальными. 
void main(){

// Варианты 
  //1) Создание словаря:
    Map < String, String > values = {
      'k1' : 'value1',
      'k2' : 'value2',
      'k3' : 'value3',
    };

  // 2) создаем пустой map и в него добавляем ключ значение, иными словами Конструктор Map():
    var films = Map();
    films['first'] = 'LOTR';
    films['second'] = 'HOBBIT';
    films['third'] = 'STAR WARS';
    films['fourth'] = 'ASOKA';

  // 3) Указывая типы данных:
    // Map<String, String> map = {
    //    'key1': 'value1',
    //    'key2': 'value2',
    //    'key3': 'value3',
    //  };

  // 4) Использование конструктора Map.from():
    //var map = Map.from({
    //    'key1': 'value1',
    //    'key2': 'value2',
    //    'key3': 'value3',
    //  });

  // 5. Использование конструктора Map.of():
    //  var map = Map.of({
    //    'key1': 'value1',
    //    'key2': 'value2',
    //    'key3': 'value3',
    //  });

  // 6. Использование метода fromIterable():

    //  var list = ['key1', 'key2', 'key3'];
    //  var map = Map.fromIterable(list, key: (k) => k, value: (v) => 'value');


// УПРАВЛЕНИЯ И МАНИПУЛЯЦИИ ДАННЫМИ В MAP
  
  //1. Добавление и удаление элементов:
    films['key'] = 'value'; //Добавляет элемент в films с указанным ключом и значением.
    films.remove('key'); //Удаляет элемент из films по указанному ключу.
  
  //2. Получение значений:
    print(films['key']); // Возвращает значение в films, связанное с указанным ключом.
    print(films.values); // Возвращает коллекцию всех значений в films.
    print(films.keys); // Возвращает коллекцию всех ключей в films.
  
  //3. Проверка наличия и поиск элементов:
   films.containsKey('key'); // Возвращает true, если films содержит элемент с указанным ключом.
   films.containsValue('value'); // Возвращает true, если films содержит элемент с указанным значением.
   films.entries; // Возвращает коллекцию пар "ключ-значение" для итерации по films.
   films.isEmpty; // Возвращает true, если films пустой.
  
  //4. Итерация по films: (подробнее будем изучать это на практике)
    //films.forEach((key, value) => ...); // Итерируется по каждой паре "ключ-значение" в films.
    //for (var entry in films.entries) { ... }; // Использует цикл for-in для итерации по films.
  
  //5. Получение информации о количестве элементов: 
   films.length; // Возвращает количество элементов в Map.

  //6. Копирование films: (подробнее будем изучать это на практике)
    //Копирование Map с помощью конструктора Map.from()
    Map<String, String> map2 = Map.from(films);
    print(map2);  

  //7. Преобразование films в другие типы: (подробнее будем изучать это на практике)
   films.keys.toList(); // Преобразует все ключи films в список.
   films.values.toList(); // Преобразует все значения films в список.
   films.entries.toList(); //  Преобразует все пары "ключ-значение" films в список.

   



  print(values);
  print(films);

}