 // Колекция множества(она же Sets)


 void main(){
  // Коллекция "множество" (Set) представляет собой неупорядоченную коллекцию уникальных элементов.
  // Это значит, что в Set не могут быть повторяющиеся элементы, и порядок элементов в Set не гарантирован.
  
  // Для создания множества в Dart существует несколько способов:
  
  // 1. Использование литерала Set:

  Set<String> fruitsOne = {'apple', 'banana', 'orange'};
  print(fruitsOne);

  // 2. Использование конструктора Set:

  var fruitsTwo = Set<String>();
  fruitsTwo.add('apple');
  fruitsTwo.add('banana');
  fruitsTwo.add('orange');

  // К множеству (Set) в Dart можно применять различные Методы. Некоторые из них включают:

  // 1. Метод add(): добавляет элемент в множество.

  fruitsTwo.add('kiwi');

  // 2. Метод addAll(): добавляет все элементы из другого итерируемого объекта в множество.

  var additionalFruits = ['mango', 'peach'];
  fruitsTwo.addAll(additionalFruits); 
  // или так:
  // fruitsTwo.addAll({'mango', 'peach'})

  // 3. Метод remove(): удаляет элемент из множества.

  fruitsTwo.remove('banana');

  // 4. Метод contains(): проверяет, содержит ли множество определенный элемент.

  print(fruitsTwo.contains('apple')); // Выведет: true

  // 5. Метод length: возвращает количество элементов в множестве.

  print(fruitsTwo.length); // Выведет: 5 

  // 6. Метод isEmpty(): проверяет, является ли множество пустым.

  print(fruitsTwo.isEmpty); // Выведет: false

  // 7. Метод isNotEmpty(): проверяет, содержит ли множество хотя бы один элемент.

  print(fruitsTwo.isNotEmpty);// Выведет: true

  // 8. Метод clear(): удаляет все элементы из множества, делая его пустым.

  fruitsTwo.clear();

  // 9. Метод forEach(): выполняет указанную функцию для каждого элемента множества.

  fruitsTwo.forEach((fruit) {
    print(fruit); 
  });

  // 10. Метод union(): возвращает новое множество, состоящее из элементов двух множеств.

  var moreFruits = {'grape', 'kiwi'};
  var allFruits = fruitsTwo.union(moreFruits);
  print(allFruits);
  // Это некоторые из Методов и методов, доступных для работы с множествами в Dart. Они позволяют добавлять, удалять, проверять наличие элементов, а также выполнять другие операции для работы с множествами.

}